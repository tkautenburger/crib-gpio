GPIO Controller für Weihnachtskrippe
====================================

Dieses Git-Repository enthält alle notwendigen Quellcodes zur Erstellung des
GPIO Controllers für eine Weihnachtskrippensteuerung auf Basis eines 
Raspberry PI Einplatinen-Computers. 


Container Image laden und starten
---------------------------------

Das fertige Docker-Image zum Herunterladen aus dem Docker-Registry finden Sie 
hier:

```
docker pull registry.gitlab.com/tkautenburger/crib-gpio/gpio-crib-arm:latest
```

Nach Download des Docker-Image können Sie den GPIO Controller auf einem 
Raspberry PI mit Hypriotos Betriebssystem folgendermaßen starten:

```
docker run -d --name gpio-crib -p 5556:5556 \
  -v /sys:/sys -v /home/pirate:/config \
  -e GPC_PORT=5556 -e MQTT_IP=172.17.0.1 \
  --restart always \
  registry.gitlab.com/tkautenburger/crib-gpio/gpio-crib-arm:latest
```

Erklärungen:
- Der GPIO Controller kommuniziert mit der Frontend GUI Applikation via REST API auf Port 5556. Dieser Port muss entsprechend mit der Option -p freigegeben werden. Optional kann auch ein anderer Port verwendet werden, dann bitte die Umgebungsvariable GPC_PORT ebenfalls auf den gewünschten Port anpassen
- Das /sys Filesystem des Raspberry PI muss als Volume für den Container freigegeben werden, da hierüber die GPIO Pins gesteuert werden
- Im /home/pirate HOME-Verzeichnis des Default-Users pirate auf dem Raspberry muss sich der `gpio-config.json` Config-File befinden. Dieser wird im Container auf das Verzeichnis `/config` gemapped. Optional kann hier auf dem Docker-Host auch ein beliebiges anderes Verzeichnis gewählt werden.
- Umgebungsvariablen: `GPC_PORT`: Kommunikationsport des GPIO Controllers, `MQTT_IP`: IP-Adresse des MQTT Message Brokers. Dieser wird i.d.R. auf dem gleichen Host installiert, deshalb kann die `IP 172.17.0.1` hier verwendet werden.
- Der Container wird immer automatisch neu gestartet, sollte er aus irgendeinem Grund mal abstürzen (--restart Option)

Für alle, die das komplette Szenario mit GPIO Controller, Angular Web Frontend Application und Mosquitto Message Broker komplett starten möchten, ist im Repository
der Docker Compose File `docker-compose-crib.yml` beigelegt. Nach wie vor gilt auch hier, dass alle Voraussetzungen zum Start der Services gegeben sein müssen, d.h.
sowohl die Konfigurationsdatei des GPIO-Controllers als auch die Konfiguration und die Pfade für den Message Broker müssen vorher entsprechend angelegt werden.
Der Compose File wird mit dem Kommando `docker-compose -f docker-compose-crib.yml up`gestartet. Dieser lädt dann die Container-Images aus den entsprechenden Registry
Einträgen und startet die Container in der Docker Runtime.

```
version: '2'
services:
#
# ----------------------------------------------------------
#  Mosquitto MQTT message broker
# ----------------------------------------------------------
#
  mosquitto:
    image: registry.gitlab.com/tkautenburger/crib-gpio/mosquitto-arm:1.4.15
    restart: always
    ports:
      - "1883:1883"
      - "9001:9001"
    volumes:
      - /usr/local/mosquitto/config/mosquitto.conf:/mosquitto/config/mosquitto.conf
      - /usr/local/mosquitto/data:/mosquitto/data
      - /usr/local/mosquitto/log:/mosquitto/log 
#
# ----------------------------------------------------------
#  GPIO controller service
# ----------------------------------------------------------
#
  gpio-crib:
    image: registry.gitlab.com/tkautenburger/crib-gpio/gpio-crib-arm:latest
    restart: always
    depends_on:
      - mosquitto
    ports:
      - "5556:5556"
    volumes:
      - /sys:/sys
      - /home/pirate:/config
    environment:
      GPC_PORT: 5556
      MQTT_IP: 172.17.0.1
#
# ----------------------------------------------------------
#  Angular web frontend application
# ----------------------------------------------------------
#
  gui-crib:
    image: registry.gitlab.com/tkautenburger/crib-control/crib-arm:latest
    restart: always
    depends_on:
      - mosquitto
      - gpio-crib
    ports:
      - "8080:8080"
```

Controller konfigurieren
----------------------------
Der GPIO Controller lädt nach dem Start eine Konfigurationsdatei, auf deren Basis die genutzten GPIO Kontakte eingestellt werden und weitere Parameter, welche an die Frontend-GUI zur Darstellung weitergegeben werden.
Die Standard-Konfiguration hat folgendes Format:

```
{
    "title": "Weihnachtskrippe",
    "controls": [
        {
            "name": "Haus",
            "pin": 23,
            "status": 1
        },
        {
            "name": "Stall",
            "pin": 24,
            "status": 0
        },
        {
            "name": "Feuerstelle",
            "pin": 25,
            "status":0
        }
    ]
}
```
Hier wird der Titel der Web-GUI festgelegt und 3 Aktoren definiert, die jeweils auf den GPIO PIN-Ausgängen 23, 24 und 25 geschaltet werden. Mit Status 1 wird festgelegt ob der Kontakt eingeschaltet gestartet werden soll, mit Status 0 ist der Kontakt initial ausgeschaltet.
Im gezeigten Beispiel wäre der GPIO 23 mit der Bezeichnung "Haus" gleich nach dem Start des Controllers aktiv, die anderen inaktiv. Die Liste der Controls kann
beliebig erweitert werden. Die Ausnahme bildet der GPIO 26 sowie der GPIO 19. Diese können nicht für weitere Aktoren verwendet werden, da diese für den Hauptschalter als Ein- und Ausgang
bereits Verwendung gefunden haben. 


Container Image selbst bauen
----------------------------
Wer sich lieber sein eigenes Docker-Image basteln will, dem sei der beigefügte Dockerfile im Repository ans Herz gelegt. Das Image kann wie folgt erzeugt werden:

```
docker build -t <my-image-path> -f Dockerfile .
```

Das Kommando muss im gleichen Verzeichnis ausgeführt werden, in dem sich `Dockerfile`und `gpio-crib.js`befinden.



MQTT Broker
----------------------------
Der GPIO Controller meldet den Status der einzelnen Aktoren via MQTT (via Websockets) an die Front-End Applikation zurück, so dass der aktuelle Status der GPIO Pins in der GUI dargestellt werden kann.
Als kleiner, schlanker MQTT Message Broker für den Raspberry wird das offizielle Mosquitto ARM Docker-Image empfohlen. Das offizielle Image kann [hier](https://hub.docker.com/r/_/eclipse-mosquitto/) vom Docker Hub entsprechend heruntergeladen werden.
Ich habe das Image aber auch diesem Projekt beigefügt:

```
docker pull registry.gitlab.com/tkautenburger/crib-gpio/mosquitto-arm:1.4.15
```

Zum Start des Docker Containers für den Mosquitto ist folgendes Kommando ans Herz gelegt:

```
docker run --name mosquitto -d --restart always \
  -p 1883:1883 -p 9001:9001 \
  -v /usr/local/mosquitto/config/mosquitto.conf:/mosquitto/config/mosquitto.conf \
  -v /usr/local/mosquitto/data:/mosquitto/data \
  -v /usr/local/mosquitto/log:/mosquitto/log \
  registry.gitlab.com/tkautenburger/crib-gpio/mosquitto-arm:1.4.15
```

Die Verzeichnisse unter `/usr/local/mosquitto` müssen alle im Vorfeld als Root-User angelegt werden. Ebenfalls ist die Datei `/usr/local/mosquitto/config/mosquitto.conf` zu erzeugen und eine leere Log-Datei unter `/usr/local/mosquitto/log/mosquitto.log` mit `sudo touch /usr/local/mosquitto/log/mosquitto.log` . Es kann natürlich ein beliebig anderes Verzeichnis auf dem Docker-Host gewählt werden.

Hier erfolgt ein funktionierendes und denkbar einfaches Beispiel für die Mosquitto Konfigurationdatei in `/usr/local/mosquitto/config/mosquitto.conf`:

```
persistence true
persistence_location /mosquitto/data/

listener 1883

listener 9001
protocol websockets

log_dest file /mosquitto/log/mosquitto.log
```


Schaltplan
----------------------------
Die Schaltung, welche über die GPIOs des Raspberry PI gesteuert wird, ist denkbar einfach. Es gibt je nach Konfiguration eine Anzahl von Ausgängen sowie eine Schaltung für einen Hauptschalter (Taster), mit dem, wenn es mal schnell gehen muss, und das Handy ist nicht zur Hand, alle Aktoren aus oder auch wieder eingeschaltet werden können.
Hier der Schaltplan, der für den GPIO Controller in der Standard-Konfiguration verwendet wurde:

![Schaltplan Krippe](Schaltplan-Krippe.png)

![Breadboard](crib-fritzing.png)

Viel Spaß,
Thomas Kautenburger