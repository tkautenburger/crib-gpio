# FROM arm32v7/node
FROM node:12
#RUN apt-get update
#RUN apt-get install -y sudo

RUN npm install express --save
RUN npm install body-parser --save
RUN npm install cookie-parser --save
RUN npm install multer --save
RUN npm install onoff --save
RUN npm install mqtt --save

ADD gpio-crib.js /gpio-crib.js

ENTRYPOINT ["node", "/gpio-crib.js"]
