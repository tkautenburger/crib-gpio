// GPIO controller
// Thomas Kautenburger, 03.10.2018

var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
var fs = require('fs');

// configuration file path
var filename = "/config/gpio-config.json";

// GPIO object array for REST controller
var GPIOs = [];

// sanity check for min and max  gpio numbers
const GPIO_MIN = 2
const GPIO_MAX = 26;

// GPIO object for master switch input
var master_in_gpio;
// GPIO object for master switch output
var master_out_gpio;

// GPIO pin numbers for main switch input and output
const GPIO_SWITCH_IN  = 26;
const GPIO_SWITCH_OUT  = 19;

// check interval for master switch in milliseconds
const CHECK_INTERVAL = 2000;

// current state of master switch, start with on an last configuration set
var MASTER_SWITCH = true;

// Default configuration, if the config file is not present, the
// GPIO controller comes up with this default configuration

// array for default configuration controls
var array = [];

// default configuration
room1 = new Object();
room1.name = "Haus";
room1.pin = 23;
room1.status = 0;
array.push(room1);

room2 = new Object();
room2.name = "Stall";
room2.pin = 24;
room2.status = 0;
array.push(room2);

room3 = new Object();
room3.name = "Feuerstelle";
room3.pin = 25;
room3.status = 0;
array.push(room3);

var defaultConfig = new Object();
defaultConfig.title = "Weihnachtskrippe";
defaultConfig.controls = array;


// Port to communicate with this service must be set in docker container
// environment variable GPC_PORT. Typically this is 5556
var PORT = process.env.GPC_PORT;
var DEFAULT_PORT = 5556;
if (PORT == undefined || PORT == '') {
  PORT = DEFAULT_PORT;
}

var MQTT_IP = process.env.MQTT_IP;
// this is typically the IP address of the docker host
var DEFAULT_MQTT_IP = '172.17.0.1'
if (MQTT_IP == undefined || MQTT_IP == '') {
  MQTT_IP = DEFAULT_MQTT_IP;
}

// Get configuration from file or if not existent use default config
var globConfig = readConfig();
if (globConfig == undefined || globConfig == null) {
  globConfig = defaultConfig;
}
// Create GPIO objects
gpioAllInit();

// Set initial state for all outgoing GPIOs
globConfig.controls.forEach(function(element){
  gpioSet(element.pin, element.status);
});


/*****************************************************
 * GPIO watcher for main on/off switch
 *****************************************************/

master_in_gpio.watch((err, value) => {
  if (err) {
    console.log("Received error while watching master switch: " + err.toString());
  }
  // toggle control light and master switch
  gpioToggle(master_out_gpio);
  MASTER_SWITCH = !MASTER_SWITCH;
});


/*****************************************************
 * interval handling
 *****************************************************/

// check every N second the status of the main switch
var intervalId;
intervalId = setInterval( function(){
  if (MASTER_SWITCH  === false) {
    // Master switch turned down
    globConfig.controls.forEach( function(element) {
      if (element.status === 1) {
        gpioDown(element.pin);
      }
      client.publish(TOPIC_OFF, element.pin.toString());
    });
  } else if (MASTER_SWITCH  === true) {
    // Master switch turned up, get all pins up that where up due to
    // latest configuration seetings
    globConfig.controls.forEach( function(element) {
      if (element.status === 1) {
        // GPIO is high according to config, so get it back up
        gpioUp(element.pin);
        client.publish(TOPIC_ON, element.pin.toString());
      }
    });
  }
}, CHECK_INTERVAL);


/*****************************************************
 * MQTT handling
 *****************************************************/

 var express = require('express');
 var app = express();

// MQTT topics for switching events
var TOPIC_OFF = 'gpc/lightOff';
var TOPIC_ON = 'gpc/lightOn';

var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://' + MQTT_IP);

// Listen to light on and light off events
client.on('connect', function() {
  client.subscribe(TOPIC_OFF, function(err) {
    if (!err) {
      console.log("MQTT listen on topic ", TOPIC_OFF);
    } else {
      console.log("Error while connecting to MQTT broker: ", err);
    }
  })
  client.subscribe(TOPIC_ON, function(err) {
    if (!err) {
      console.log("MQTT listen on topic ", TOPIC_ON);
    } else {
      console.log("Error while connecting to MQTT broker: ", err);
    }
  })
})

function gpioDown(pin) {
  const result = GPIOs.find(element => element.pin == parseInt(pin));
  if (result != undefined) {
    gpioLow(result.gpio);
  }
}

function gpioUp(pin) {
  const result = GPIOs.find(element => element.pin == parseInt(pin));
  if (result != undefined) {
    gpioHigh(result.gpio);
  }
}

function gpioSet(pin, status) {
  const result = GPIOs.find(element => element.pin == parseInt(pin));
  if (result != undefined) {
    if (status == 0) {
      gpioLow(result.gpio);
    } else {
      gpioHigh(result.gpio);
    }
  }
}


/*****************************************************
 * REST handling
 *****************************************************/

/*
 * Needed to deal with CORS requirements from Browser's Angular
 * Javascript App. Allow for GET requests from arbitrary IP addresses
 */
app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header('Access-Control-Allow-Methods', 'GET');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});


app.get('/gpioHigh', function(req, res) {
  let pin = req.query.pin;
  let obj = new Object();
  obj.pin = pin;
  if (MASTER_SWITCH && pin >= GPIO_MIN && pin <= GPIO_MAX) {
    const result = GPIOs.find(element => element.pin == pin);
    if (result == undefined) {
      obj.status = -1;
    } else {
      gpioHigh(result.gpio);
      client.publish(TOPIC_ON, result.pin.toString());
      obj.status = gpioStatus(result.gpio);
      updateConfig(globConfig, result.pin, obj.status);
    }
  } else {
    obj.status = -1;
  }
  let resp = JSON.stringify(obj);
  res.send(resp);
})


app.get('/gpioLow', function(req, res) {
  let pin = req.query.pin;
  let obj = new Object();
  obj.pin = pin;
  if (MASTER_SWITCH && pin >= GPIO_MIN && pin <= GPIO_MAX) {
    const result = GPIOs.find(element => element.pin == pin);
    if (result == undefined) {
      obj.status = -1;
    } else {
      gpioLow(result.gpio);
      client.publish(TOPIC_OFF, result.pin.toString());
      obj.status = gpioStatus(result.gpio);
      updateConfig(globConfig, result.pin, obj.status);
    }
  } else {
    obj.status = -1;
  }
  let resp = JSON.stringify(obj);
  res.send(resp);
})


app.get('/gpioStatus', function(req, res) {
  let pin = req.query.pin;
  let obj = new Object();
  obj.pin = pin;
  if (pin >= GPIO_MIN && pin <= GPIO_MAX) {
    const result = GPIOs.find(element => element.pin == pin);
    if (result == undefined) {
      obj.status = -1;
    } else {
      obj.status = gpioStatus(result.gpio);
    }
  } else {
    obj.status = -1;
  }
  let resp = JSON.stringify(obj);
  res.send(resp);
})


app.get('/gpioConfig', function(req, res) {
  res.send(globConfig);
})


var server = app.listen(PORT, function(req, res) {
  var port = server.address().port;
  console.log("GPIO Controller is listening at port %s", port);
})


/*****************************************************
 * General helper functions
 *****************************************************/

// Clean up when stopping the container
function unexportOnClose() { //function to stop blinking
  console.log("GPIO controller is going down. Turning down GPIOs");
  GPIOs.map(obj => {
    gpioLow(obj.gpio);
    obj.gpio.unexport();
  });
  gpioLow(master_out_gpio);
  master_out_gpio.unexport();
  master_in_gpio.unexport();
  client.end();
  process.exit(0);
}

// Handle some signals that shutdown the service
process.on('SIGINT', unexportOnClose); // function to run when user closes with ctrl+c
process.on('SIGTERM', unexportOnClose); // function to run when process gets TERM signal


// Set GPIO with the given GPIO object to 1
function gpioHigh(gpio) {
  if (gpio.readSync() == 0) {
    gpio.writeSync(1);
  }
}

// Set GPIO with the given GPIO object to 0
function gpioLow(gpio) {
  if (gpio.readSync() == 1) {
    gpio.writeSync(0);
  }
}

// Toggle GPIO
function gpioToggle(gpio) {
  gpio.writeSync(gpio.readSync() ^1);
}

// Get the status from the given GPIO object
function gpioStatus(gpio) {
  let state = gpio.readSync();
  return state;
}

// Initialize the GPIO with the GPIO number to input or output contact
// and return the GPIO object
function gpioInit(pin, direction) {
  return new Gpio(pin, direction);
}

// Initialiye all GPIO object received from the configuration and
// Initialize the contacts for master switch
function gpioAllInit() {
  globConfig.controls.forEach( function(element) {
    let gpioObj = new Object();
    gpioObj.pin = element.pin;
    gpioObj.gpio = gpioInit(element.pin, 'out');
    GPIOs.push(gpioObj);
  });

  // set the master in switch
  master_in_gpio  = new Gpio(GPIO_SWITCH_IN, 'in', 'rising', {debounceTimeout: 10});

  // set the master light out gpio and we start with ON
  master_out_gpio = gpioInit(GPIO_SWITCH_OUT, 'out');
  gpioHigh(master_out_gpio);
}

/*****************************************************
 * File IO functions
 *****************************************************/

// Write  config file to disk
function writeConfig(config) {
  fs.writeFileSync(filename, JSON.stringify(config, null, 4));
}

// Update config file with new GPIO status
function updateConfig(config, pin, status) {
  for (let element of config.controls) {
    if (element.pin === pin) {
      element.status = status;
      break;
    }
  }
  fs.writeFileSync(filename, JSON.stringify(config, null, 4));
}

// Read config file from disk
function readConfig() {
  if (fs.existsSync(filename)) {
    let rawdata = fs.readFileSync(filename);
    return JSON.parse(rawdata);
  }
  return null;
}
